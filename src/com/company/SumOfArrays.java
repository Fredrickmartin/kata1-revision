package com.company;

public class SumOfArrays{
    public static void main(String args[]){

        int array [] = {20,30, 80,12,65,78,96};

        int sum = 0;

        for (int num : array){

            sum = sum + num;

        }
           System.out.println("Sum of array elements is :" + sum);

    }
}

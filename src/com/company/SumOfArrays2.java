package com.company;

public class SumOfArrays2 {
    public static void main(String[] args) {

        // Exercise 2: Sum of numbers
        //
        //This task requires you to sum all numbers in an array: (324,452,12,4,55,20,1,87)
        int array [] = {324, 452, 12, 4, 55, 20, 1, 87};
        int sum = 0;
             for (int num : array){
                 sum = sum + num;
             }

             System.out.println("Sum of elements array is :" + sum);
    }
}

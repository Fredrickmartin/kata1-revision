package com.company;

public class OddNumbers {

    public static void main(String[] args) {

        //Exercise 4: All odds
        //
        //Write a function to print the odd numbers from 1 to 99.

        for ( int i = 1; i < 100; i++){

            if (i % 2 != 0){

                System.out.println(i);
            }
        }

    }
}
